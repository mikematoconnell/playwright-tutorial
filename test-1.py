from playwright.sync_api import Playwright, sync_playwright, expect
import json
import os

local = os.environ.get("local")
local_value = bool(local)

def run(playwright: Playwright) -> None:
    browser = playwright.chromium.launch(headless=local_value)
    context = browser.new_context()
    page = context.new_page()
    page.goto("https://michaelcrwnclothing.netlify.app/")
    page.get_by_role("link", name="Sign-In").click()
    page.locator("form").filter(has_text="EmailPasswordSign InGoogle Sign In").locator("input[name=\"email\"]").click()
    page.locator("form").filter(has_text="EmailPasswordSign InGoogle Sign In").locator("input[name=\"email\"]").fill("mikegg@gmail.com")
    page.locator("form").filter(has_text="EmailPasswordSign InGoogle Sign In").locator("input[name=\"password\"]").click()
    page.locator("form").filter(has_text="EmailPasswordSign InGoogle Sign In").locator("input[name=\"password\"]").fill("ABC123")
    
    # gets the locator associated with password
    password_locator = page.locator("form").filter(has_text="EmailPasswordSign InGoogle Sign In").locator("input[name=\"password\"]")
    # gets the value of the password from the password locator (uses .value() instead of .text_content() because it is an input field)
    # more information below
    password_value = password_locator.get_attribute("value")
    # asserts that the value inputted by the test matches the value got from the password_locator
    assert (password_value == "ABC123")

    page.locator("form").filter(has_text="EmailPasswordSign InGoogle Sign In").locator("input[name=\"password\"]").press("Enter")
    page.locator("form").filter(has_text="EmailPasswordSign InGoogle Sign In").locator("input[name=\"password\"]").press("Enter")
    page.get_by_role("link", name="Shop").click()
    page.get_by_test_id("Brown Cowboy").hover(timeout=3000)
    page.get_by_role("button", name="Add to cart").click()
    page.get_by_role("button", name="Add to cart").click()
    page.get_by_text("Blue Beanie").hover(timeout=3000)
    page.get_by_role("button", name="Add to cart").dblclick()
    page.get_by_role("button", name="Add to cart").dblclick()
    
    # retrieves Local Storage
    local_storage_data = page.eval_on_selector(
        "html",
        "() => window.localStorage"
    )

    # accesses the persist:root dict key and unjosns it
    un_json_local_storage_cart = json.loads(local_storage_data["persist:root"])
    # accesses the cart key
    cart_storage = un_json_local_storage_cart["cart"]
    # de jsonifies cart storage so you can access cartItems which is a key inside of cart
    un_json_cart_storage = json.loads(cart_storage)
    # access the items values
    cart_items = un_json_cart_storage["cartItems"]
    
    # calculates the quantity of items currently in cart
    total_quantity = 0
    for item in cart_items:
        total_quantity += item["quantity"]
    
    # uses data-testid on cartIcon to get the value that is being displayed
    cart_icon_number_of_items = page.get_by_test_id("numberOfCartItems")  
    # get the text_content of the cartIcon element
    # text_content is used to get the value of an element whereas .value() is used for the value of an input element
    value_of_cart_icon = cart_icon_number_of_items.text_content()
    
    # value received is a string so will need to change to an integer
    int_value_cart_icon = int(value_of_cart_icon)
    # asserts that the value from local storage match what is being displayed on the page
    assert(total_quantity == int_value_cart_icon)

    expect(page.get_by_role("link", name="Sign-In")).to_be_hidden()
    # ---------------------
    context.close()
    browser.close()



with sync_playwright() as playwright:
    run(playwright)
